<?php
require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Marketplace\Controller\ShopControllerProvider;
use Marketplace\Controller\CartControllerProvider;

$app = new Application();
$app['debug'] = true;

$app['pagarme_key'] = 'ak_test_KW0yuiIXY0kfU4VBvbQHEP6seVSc4x';

$app->register(new DoctrineServiceProvider, [
    'db.options' => [
        'driver' => 'pdo_sqlite',
        'path' => dirname(__DIR__).'/marketplace.db',
    ],
]);

$app->register(new TwigServiceProvider(), [
    'twig.path' => dirname(__DIR__).'/src/views',
]);

$app->register(new SessionServiceProvider());

$app->mount('/shop', new ShopControllerProvider());

$app->mount('/cart', new Marketplace\Controller\CartControllerProvider());

return $app;
