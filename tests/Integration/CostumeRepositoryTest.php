<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Tests\Integration\Builders\ConnectionBuilder;
use Marketplace\Tests\Integration\Builders\CostumeRepositoryBuilder;
use Marketplace\Repository\CostumeRepository;
use Marketplace\Entity\Costume;
use Marketplace\Entity\Seller;

class CostumeRepositoryTest extends Testcase
{
    public static function setUpBeforeClass()
    {
        ConnectionBuilder::build();
    }

    public function setUp()
    {
        CostumeRepositoryBuilder::build();
    }

    public function testGetRelatedSeller()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);
        $seller = $repository->getSeller(1);

        $this->assertInstanceOf(Seller::class, $seller);

    }

    public function testCostumeCreateAndRead()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);
        $newCostume = new Costume();
        $newCostume->setCostume('Costume1Test');
        $newCostume->setPriceRent(100);
        $newCostume->setSellerId(1);

        $repository->create($newCostume);

        $costume = $repository->getOne(2);

        $this->assertEquals(2, $costume->getId());
    }

    public function testCostumeUpdate()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);

        $costumeUpdated = new Costume();
        $costumeUpdated->setId(1);
        $costumeUpdated->setCostume('changed');
        $costumeUpdated->setPriceRent(10);
        $costumeUpdated->setSellerId(2);

        $repository->update($costumeUpdated);

        $costume = $repository->getOne(1);

        $this->assertEquals('changed', $costume->getCostume());

        $this->assertEquals(10, $costume->getPriceRent());

        $this->assertEquals(2, $costume->seller()->getId());
    }

    public function testCostumeRemove()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);

        $repository->remove(1);

        $costume = $repository->getOne(1);

        $this->assertEmpty($costume);
    }

    public function testCostumeReadAllWithLimit()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);

        $newCostume = new Costume();
        $newCostume->setCostume('Costume1Test');
        $newCostume->setPriceRent(100);
        $newCostume->setSellerId(1);
        $repository->create($newCostume);

        $costumes = $repository->getAll(1);

        $this->assertInternalType('array', $costumes);
        $this->assertCount(1, $costumes);
        $this->assertInstanceOf(Costume::class, $costumes[0]);
    }

    public function testCostumeReadAllWithoutLimit()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);

        $newCostume = new Costume();
        $newCostume->setCostume('Costume1Test');
        $newCostume->setPriceRent(100);
        $newCostume->setSellerId(1);
        $repository->create($newCostume);

        $costumes = $repository->getAll();

        $this->assertInternalType('array', $costumes);
        $this->assertCount(2, $costumes);
        $this->assertInstanceOf(Costume::class, $costumes[0]);
    }

    public function testGetManyCostumesByIds()
    {
        $repository = new CostumeRepository(ConnectionBuilder::$conn);

        $newCostume = new Costume();
        $newCostume->setCostume('Costume1Test');
        $newCostume->setPriceRent(100);
        $newCostume->setSellerId(1);

        $repository->create($newCostume);

        $costumes = $repository->getMany([1,2]);

        $this->assertInternalType('array', $costumes);
        $this->assertCount(2, $costumes);
        $this->assertInstanceOf(Costume::class, $costumes[0]);
    }

    public function tearDown()
    {
        CostumeRepositoryBuilder::destroyBuild();
    }

    public static function tearDownAfterClass()
    {
        ConnectionBuilder::destroyBuild();
    }
}
