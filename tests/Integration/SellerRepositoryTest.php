<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Tests\Integration\Builders\ConnectionBuilder;
use Marketplace\Tests\Integration\Builders\SellerRepositoryBuilder;
use Marketplace\Repository\SellerRepository;
use Marketplace\Entity\Seller;

class SellerRepositoryTest extends Testcase
{
    public static function setUpBeforeClass()
    {
        ConnectionBuilder::build();
    }

    public function setUp()
    {
        SellerRepositoryBuilder::build();
    }

    public function testSellerCreateAndRead()
    {
        $repository = new SellerRepository(ConnectionBuilder::$conn);
        $newSeller = new Seller();
        $newSeller->setSeller('Seller1Test');
        $newSeller->setGainPercentage(100);

        $repository->create($newSeller);

        $seller = $repository->getOne(2);

        $this->assertEquals(2, $seller->getId());
    }

    public function testSellerUpdate()
    {
        $repository = new SellerRepository(ConnectionBuilder::$conn);

        $sellerUpdated = new Seller();

        $this->assertFalse($repository->update($sellerUpdated));

        $sellerUpdated->setId(1);
        $sellerUpdated->setSeller('changed');
        $sellerUpdated->setGainPercentage(10);

        $this->assertTrue($repository->update($sellerUpdated));

        $seller = $repository->getOne(1);

        $this->assertEquals('changed', $seller->getSeller());

        $this->assertEquals(10, $seller->getGainPercentage());
    }

    public function testSellerRemove()
    {
        $repository = new SellerRepository(ConnectionBuilder::$conn);

        $repository->remove(1);

        $seller = $repository->getOne(1);

        $this->assertEmpty($seller);
    }

    public function tearDown()
    {
        SellerRepositoryBuilder::destroyBuild();
    }

    public static function tearDownAfterClass()
    {
        ConnectionBuilder::destroyBuild();
    }
}
