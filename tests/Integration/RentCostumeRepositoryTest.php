<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Tests\Integration\Builders\ConnectionBuilder;
use Marketplace\Tests\Integration\Builders\RentCostumeRepositoryBuilder;
use Marketplace\Repository\RentCostumeRepository;
use Marketplace\Repository\RentRepository;
use Marketplace\Repository\CostumeRepository;
use Marketplace\Entity\RentCostume;
use Marketplace\Entity\Rent;
use Marketplace\Entity\Costume;

class RentCostumeRepositoryTest extends Testcase
{
    public static function setUpBeforeClass()
    {
        ConnectionBuilder::build();
    }

    public function setUp()
    {
        RentCostumeRepositoryBuilder::build();
    }

    public function testGetRelatedRent()
    {
        $repository = new RentCostumeRepository(ConnectionBuilder::$conn);
        $rent = $repository->getRent(1);

        $this->assertInstanceOf(Rent::class, $rent);
    }

    public function testGetRelatedCostume()
    {
        $repository = new RentCostumeRepository(ConnectionBuilder::$conn);
        $costume = $repository->getCostume(1);

        $this->assertInstanceOf(Costume::class, $costume);
    }

    public function testRentCostumeCreateAndReadByRentAndCostume()
    {
        $repository = new RentCostumeRepository(ConnectionBuilder::$conn);
        $newRentCostume = new RentCostume();
        $newRentCostume->setRentId(2);
        $newRentCostume->setCostumeId(2);
        $newRentCostume->setPrice(100);
        $newRentCostume->setQuantity(1);

        $repository->create($newRentCostume);

        $rent = (new RentRepository(ConnectionBuilder::$conn))
            ->getOne(2);

        $costume = (new CostumeRepository(ConnectionBuilder::$conn))
            ->getOne(2);

        $rentCostume = $repository->getByRentAndCostume($rent, $costume);

        $this->assertEquals(2, $rentCostume->getId());
    }

    public function tearDown()
    {
        RentCostumeRepositoryBuilder::destroyBuild();
    }

    public static function tearDownAfterClass()
    {
        ConnectionBuilder::destroyBuild();
    }
}
