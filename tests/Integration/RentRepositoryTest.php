<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Tests\Integration\Builders\ConnectionBuilder;
use Marketplace\Tests\Integration\Builders\RentRepositoryBuilder;
use Marketplace\Repository\RentRepository;
use Marketplace\Entity\Rent;
use Marketplace\Entity\Status;

class RentRepositoryTest extends Testcase
{
    public static function setUpBeforeClass()
    {
        ConnectionBuilder::build();
    }

    public function setUp()
    {
        RentRepositoryBuilder::build();
    }

    public function testGetRelatedStatus()
    {
        $repository = new RentRepository(ConnectionBuilder::$conn);
        $status = $repository->getStatus(1);

        $this->assertInstanceOf(Status::class, $status);
    }

    public function testRentCreateAndRead()
    {
        $repository = new RentRepository(ConnectionBuilder::$conn);
        $newRent = new Rent();
        $newRent->setStatusId(1);
        $newRent->setAmount(100);
        $newRent->setShippingPrice(20);

        $repository->create($newRent);

        $rent = $repository->getOne(2);

        $this->assertEquals(2, $rent->getId());
    }

    public function testRentReadAllWithLimit()
    {
        $repository = new RentRepository(ConnectionBuilder::$conn);

        $newRent = new Rent();
        $newRent->setStatusId(1);
        $newRent->setAmount(100);
        $newRent->setShippingPrice(20);
        $repository->create($newRent);

        $rents = $repository->getAll(1);

        $this->assertInternalType('array', $rents);
        $this->assertCount(1, $rents);
        $this->assertInstanceOf(Rent::class, $rents[0]);
    }

    public function testRentReadAllWithoutLimit()
    {
        $repository = new RentRepository(ConnectionBuilder::$conn);

        $newRent = new Rent();
        $newRent->setStatusId(1);
        $newRent->setAmount(100);
        $newRent->setShippingPrice(20);
        $repository->create($newRent);

        $rents = $repository->getAll();

        $this->assertInternalType('array', $rents);
        $this->assertCount(2, $rents);
        $this->assertInstanceOf(Rent::class, $rents[0]);
    }

    public function tearDown()
    {
        RentRepositoryBuilder::destroyBuild();
    }

    public static function tearDownAfterClass()
    {
        ConnectionBuilder::destroyBuild();
    }
}
