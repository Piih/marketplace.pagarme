<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Tests\Integration\Builders\ConnectionBuilder;
use Marketplace\Tests\Integration\Builders\StatusRepositoryBuilder;
use Marketplace\Repository\StatusRepository;
use Marketplace\Entity\Status;

class StatusRepositoryTest extends Testcase
{
    public static function setUpBeforeClass()
    {
        ConnectionBuilder::build();
    }

    public function setUp()
    {
        StatusRepositoryBuilder::build();
    }

    public function testStatusCreateAndRead()
    {
        $repository = new StatusRepository(ConnectionBuilder::$conn);
        $newStatus = new Status();
        $newStatus->setStatus('paid');

        $repository->create($newStatus);

        $status = $repository->getOne(2);

        $statusEmpty = $repository->getOne(9232);

        $this->assertEquals(2, $status->getId());
        $this->assertEmpty($statusEmpty);
    }

    public function testStatusUpdate()
    {
        $repository = new StatusRepository(ConnectionBuilder::$conn);

        $statusUpdated = new Status();

        $this->assertFalse($repository->update($statusUpdated));

        $statusUpdated->setId(1);
        $statusUpdated->setStatus('changed');

        $this->assertTrue($repository->update($statusUpdated));

        $status = $repository->getOne(1);

        $this->assertEquals('changed', $status->getStatus());
    }

    public function tearDown()
    {
        StatusRepositoryBuilder::destroyBuild();
    }

    public static function tearDownAfterClass()
    {
        ConnectionBuilder::destroyBuild();
    }
}
