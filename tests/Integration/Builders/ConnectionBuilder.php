<?php

namespace Marketplace\Tests\Integration\Builders;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class ConnectionBuilder
{
    public static $conn;

    public static function build()
    {
        $config = new Configuration();
        $connectionParams = [
            'driver' => 'pdo_sqlite',
            'path' => dirname(dirname(dirname(__DIR__))).'/marketplace_test.db',
        ];
        self::$conn = DriverManager::getConnection($connectionParams, $config);

        return self::$conn;
    }

    public static function destroyBuild()
    {
        self::$conn = null;
    }
}
