<?php

namespace Marketplace\Tests\Integration\Builders;


class StatusRepositoryBuilder
{
    public static function build()
    {
        self::add();
    }

    public static function destroyBuild()
    {
        self::truncate('status');
    }

    public static function add()
    {
        $conn = ConnectionBuilder::$conn;
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder
            ->insert('status')
            ->setValue('status', '?')
            ->setParameter(0, 'paid')
            ->setValue('created_at', '?')
            ->setParameter(1, date('Y-m-d H:i:s'))
            ->setValue('updated_at', '?')
            ->setParameter(2, date('Y-m-d H:i:s'))
            ->execute();

    }

    private static function truncate($table)
    {
        $conn = ConnectionBuilder::$conn;
        $conn->query('DELETE FROM '.$table);
        $conn->query("DElETE FROM sqlite_sequence WHERE name='{$table}'");
    }
}
