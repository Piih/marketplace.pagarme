<?php

namespace Marketplace\Tests\Integration\Builders;

class RentRepositoryBuilder
{
    public static function build()
    {
        StatusRepositoryBuilder::add();
        self::add();
    }

    public static function destroyBuild()
    {
        self::truncate('rent');
    }

    public static function add()
    {
        $conn = ConnectionBuilder::$conn;
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder
            ->insert('rent')
            ->setValue('status_id', '?')
            ->setParameter(0, 'paid')
            ->setValue('amount', '?')
            ->setParameter(1, 100)
            ->setValue('shipping_price', '?')
            ->setParameter(2, 20)
            ->setValue('created_at', '?')
            ->setParameter(3, date('Y-m-d H:i:s'))
            ->setValue('updated_at', '?')
            ->setParameter(4, date('Y-m-d H:i:s'))
            ->execute();
    }

    private static function truncate($table)
    {
        $conn = ConnectionBuilder::$conn;
        $conn->query('DELETE FROM '.$table);
        $conn->query("DElETE FROM sqlite_sequence WHERE name='{$table}'");
    }
}
