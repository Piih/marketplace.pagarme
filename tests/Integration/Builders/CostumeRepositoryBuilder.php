<?php

namespace Marketplace\Tests\Integration\Builders;


class CostumeRepositoryBuilder
{
    public static function build()
    {
        self::add();
        SellerRepositoryBuilder::add();
    }

    public static function destroyBuild()
    {
        self::truncate('costume');
    }

    public static function add()
    {
        $conn = ConnectionBuilder::$conn;
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder
            ->insert('costume')
            ->setValue('costume', '?')
            ->setParameter(0, 'Costume Test 1')
            ->setValue('price_rent', '?')
            ->setParameter(1, 100)
            ->setValue('seller_id', '?')
            ->setParameter(2, 1)
            ->setValue('created_at', '?')
            ->setParameter(3, date('Y-m-d H:i:s'))
            ->setValue('updated_at', '?')
            ->setParameter(4, date('Y-m-d H:i:s'))
            ->execute();
    }

    private static function truncate($table)
    {
        $conn = ConnectionBuilder::$conn;
        $conn->query('DELETE FROM '.$table);
        $conn->query("DElETE FROM sqlite_sequence WHERE name='{$table}'");
    }
}
