<?php

namespace Marketplace\Tests\Integration\Builders;


class SellerRepositoryBuilder
{
    public static function build()
    {
        self::add();
    }

    public static function destroyBuild()
    {
        self::truncate('seller');
    }

    public static function add()
    {
        $conn = ConnectionBuilder::$conn;
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder
            ->insert('seller')
            ->setValue('seller', '?')
            ->setParameter(0, 'Seller Test 1')
            ->setValue('gain_percentage', '?')
            ->setParameter(1, 85)
            ->setValue('created_at', '?')
            ->setParameter(2, date('Y-m-d H:i:s'))
            ->setValue('updated_at', '?')
            ->setParameter(3, date('Y-m-d H:i:s'))
            ->execute();
    }

    private static function truncate($table)
    {
        $conn = ConnectionBuilder::$conn;
        $conn->query('DELETE FROM '.$table);
        $conn->query("DElETE FROM sqlite_sequence WHERE name='{$table}'");
    }
}
