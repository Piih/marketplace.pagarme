<?php

namespace Marketplace\Tests\Integration\Builders;

class RentCostumeRepositoryBuilder
{
    public static function build()
    {
        StatusRepositoryBuilder::add();
        RentRepositoryBuilder::add();
        RentRepositoryBuilder::add();

        SellerRepositoryBuilder::add();
        CostumeRepositoryBuilder::add();
        CostumeRepositoryBuilder::add();

        self::add();
    }

    public static function destroyBuild()
    {
        self::truncate('status');
        self::truncate('rent');
        self::truncate('seller');
        self::truncate('costume');
        self::truncate('rent_costume');
    }

    private static function add()
    {
        $conn = ConnectionBuilder::$conn;
        $queryBuilder = $conn->createQueryBuilder();
        $queryBuilder
            ->insert('rent_costume')
            ->setValue('rent_id', '?')
            ->setParameter(0, 1)
            ->setValue('costume_id', '?')
            ->setParameter(1, 1)
            ->setValue('price', '?')
            ->setParameter(2, 100)
            ->setValue('quantity', '?')
            ->setParameter(3, 1)
            ->setValue('created_at', '?')
            ->setParameter(4, date('Y-m-d H:i:s'))
            ->setValue('updated_at', '?')
            ->setParameter(5, date('Y-m-d H:i:s'))
            ->execute();

    }

    private static function truncate($table)
    {
        $conn = ConnectionBuilder::$conn;
        $conn->query('DELETE FROM '.$table);
        $conn->query("DElETE FROM sqlite_sequence WHERE name='{$table}'");
    }
}
