<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\BaseEntity;

class BaseEntityTest extends Testcase
{
    public function testEntityFilledByArrayValue()
    {
        $baseEntity = new BaseEntity();
        $baseEntity->id = null;
        $baseEntity->name = null;

        $baseEntity->fillWithArray([
            'id' => 1,
            'name' => 'entity',
        ]);

        $this->assertEquals(1, $baseEntity->id);
        $this->assertEquals('entity', $baseEntity->name);
    }
}
