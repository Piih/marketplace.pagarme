<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\Rent;
use Marketplace\Entity\Costume;
use Marketplace\Entity\RentCostume;
use Marketplace\Repository\RentCostumeRepository;

class RentCostumeTest extends Testcase
{
    public function testRentCostumeCreationAllParams()
    {
        $stubRepository = $this->getMockBuilder(RentCostumeRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stubRepository->method('getRent')
            ->willReturn(new Rent());

        $stubRepository->method('getCostume')
            ->willReturn(new Costume());

        $rentCostume = new RentCostume(1,1,1,1);

        $rentCostume->setRepository($stubRepository);

        $this->assertInstanceOf(RentCostume::class, $rentCostume);
        return $rentCostume;
    }

    /**
     * @depends testRentCostumeCreationAllParams
     */
    public function testGetRentAndCostume(RentCostume $rentCostume)
    {
        $this->assertInstanceOf(Rent::class, $rentCostume->rent());
        $this->assertInstanceOf(Costume::class, $rentCostume->costume());
    }

    public function testRentCostumeCreationWithoutParams()
    {
        $rentCostume = new RentCostume();
        $this->assertInstanceOf(RentCostume::class, $rentCostume);
        return $rentCostume;
    }
}
