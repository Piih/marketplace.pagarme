<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\Costume;
use Marketplace\Entity\Seller;
use Marketplace\Repository\CostumeRepository;
use Marketplace\Repository\SellerRepository;

class CostumeTest extends Testcase
{
    public function testCostumeCreationAllParams()
    {
        $costume = new Costume(1,'Costume1', 100, 1);
        $this->assertInstanceOf(Costume::class, $costume);
        return $costume;
    }

    public function testCostumeCreationWithoutParams()
    {
        $costume = new Costume();
        $this->assertInstanceOf(Costume::class, $costume);
        return $costume;
    }

    /**
     * @depends testCostumeCreationAllParams
     */
    public function testCostumeRelationMethods(Costume $costume)
    {
        $stubRepository = $this->getMockBuilder(CostumeRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stubRepository->method('getSeller')
            ->willReturn(new Seller());

        $costume->setRepository($stubRepository);

        $this->assertInstanceOf(Seller::class, $costume->seller());
    }

}
