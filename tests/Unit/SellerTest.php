<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\Seller;

class SellerTest extends Testcase
{
    public function testSellerCreationAllParams()
    {
        $seller = new Seller(1,'Seller1', 85);
        $this->assertInstanceOf(Seller::class, $seller);
        return $seller;
    }

    public function testSellerCreationWithoutParams()
    {
        $seller = new Seller();
        $this->assertInstanceOf(Seller::class, $seller);
        return $seller;
    }
}
