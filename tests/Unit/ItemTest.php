<?php

namespace Marketplace\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\Item;
use Marketplace\Entity\Seller;
use Marketplace\Entity\Costume;

class ItemTest extends Testcase
{
    public function testItemCreationAllParams()
    {
        $stub = $this->getMockBuilder(Costume::class)
            ->getMock();

        $item = new Item($stub);

        $this->assertInstanceOf(Item::class, $item);

        return $item;
    }

    /**
     * @depends testItemCreationAllParams
     */
    public function testSpliProfitForSellers(Item $item)
    {
        $stubSeller = $this->getMockBuilder(Seller::class)
            ->getMock();
        $stubSeller->method('getId')
            ->willReturn(1);

        $stubOtherSeller = $this->getMockBuilder(Seller::class)
            ->getMock(); 
        $stubOtherSeller->method('getId')
            ->willReturn(2);

        $stubMasterSeller = $this->getMockBuilder(Seller::class)
            ->getMock();
        $stubMasterSeller->method('getId')
            ->willReturn(1);

        $this->assertNull($item->splitProfitForSellers($stubSeller, $stubMasterSeller));

        $this->assertNull($item->splitProfitForSellers($stubOtherSeller, $stubMasterSeller));
    }
}
