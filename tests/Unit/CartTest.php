<?php

namespace Marketplace\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Marketplace\Entity\Cart;
use Marketplace\Entity\Item;
use Marketplace\Entity\Costume;
use Symfony\Component\HttpFoundation\Session\Session;

class CartTest extends Testcase
{
    public function testCanCreateCart()
    {
        $stub = $this->getMockBuilder(Session::class)
            ->getMock();

        $cart = new Cart($stub);

        $this->assertInstanceOf(Cart::class, $cart);

        return $cart;
    }

    /**
     * @depends testCanCreateCart
     */
    public function testAddAndGetCartItens(Cart $cart)
    {
        $stubCostume = $this->getMockBuilder(Costume::class)
            ->getMock(); 
        $stubCostume->method('getPriceRent')
            ->willReturn(100); 
        $stubCostume->method('getId')
            ->willReturn(1);

        $stubOtherCostume = $this->getMockBuilder(Costume::class)
            ->getMock(); 
        $stubOtherCostume->method('getPriceRent')
            ->willReturn(100); 
        $stubOtherCostume->method('getId')
            ->willReturn(2);

        $stubItem = $this->getMockBuilder(Item::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stubItem->method('getQuantity')
            ->willReturn(1);
        $stubItem->method('getShippingPrice')
            ->willReturn(10);
        $stubItem->method('getCostume')
            ->willReturn($stubCostume);

        $stubOtherItem = $this->getMockBuilder(Item::class)
            ->disableOriginalConstructor()
            ->getMock();
        $stubOtherItem->method('getQuantity')
            ->willReturn(1);
        $stubOtherItem->method('getShippingPrice')
            ->willReturn(10);
        $stubOtherItem->method('getCostume')
            ->willReturn($stubOtherCostume);

        $this->assertTrue($cart->addItem($stubItem));
        $this->assertFalse($cart->addItem($stubItem));

        $cart->addItem($stubOtherItem);

        $this->assertInternalType('array', $cart->getItens());
        $this->assertCount(2, $cart->getItens());
        $this->assertInstanceOf(Item::class, $cart->getItens()[0]);
        $this->assertEquals(1, $cart->getItens()[0]->getQuantity());

        return $cart;
    }

    /**
     * @depends testAddAndGetCartItens
     */
    public function testSumItensWithAndWithoutShipping(Cart $cart)
    {
        $this->assertEquals(200, $cart->sumItensPrices());
        $this->assertEquals(220, $cart->sumItensPricesWithShipping());
    }


}
