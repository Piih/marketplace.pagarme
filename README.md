# Marketplace #

****That version do not persists the transaction on DB****.

### System Requirements ###

* Apache 2.4 - *Enabled mod_rewrite*
* PHP 5.6 - *Enabled php SQLite*

### Setting up ###

Run composer for PHP dependencies:
```shell
$ composer install
```
Run bower for JavaScript dependencies:
```shell
$ bower install
```

### Running Tests ###

I used PHPUnit 5.7 and CodeCoverage 4.0.4(*Optional*)
```shell
$ vendor/bin/phpunit tests/
```

### Running Appliction ###

After setting up the application, just access http://{*base_url*}/shop