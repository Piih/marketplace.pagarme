const main = {
    refreshCart : () => new Promise((resolve, reject) => {
        $.get(
            '/cart/get-itens',
            response => {
                $('#cart-count').html(Object.keys(response).length)
                resolve(response)
            })
    }),

    displayAlertIn : (querySelector, message, type) => {
        $(querySelector).prepend(
            '<div class="alert alert-'+type+' alert-dismissable">'+
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+
            message +
            '</div>'
        )
    }

}

main.refreshCart()
