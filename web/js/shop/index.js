const addToCart = costumeId => new Promise((resolve, reject) => {
    $.post(
        '/cart/add-costume',
        { 'costume-id': costumeId },
        response => {
            response
                ? main.displayAlertIn('#alert-box', 'Sucesso! Fantasia adicionada ao carrinho.','success')
                : main.displayAlertIn('#alert-box', 'Atenção! O item não foi adicionado pois já existe no carrinho.', 'warning')

            resolve(response)
        })
})

$('.costume-box').on('click', function(e) {
    e.preventDefault()

    const costumeId = $(this).attr('data-costume-id')

    const originalText = $(this).find('.btn-rent').html()

    $(this).find('.btn-rent').html('Adicionando...')

    addToCart(costumeId)
        .then(main.refreshCart)
        .then(response => {
            $(this).find('.btn-rent').html(originalText)
        })
})
