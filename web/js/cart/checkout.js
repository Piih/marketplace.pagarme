const canCheckout = () => {
    if($('#card-number').val() == '') return false

    if($('#holder-name').val() == '') return false

    if($('#month').val() == '') return false

    if($('#year').val() == '') return false

    return true
}

$('#checkout-button').on('click', function(e) {
    if(!canCheckout()) {
        main.displayAlertIn('#alert-box', 'Atenção! Todos os campos devem ser preenchidos.','danger')
        e.preventDefault()
        return
    }

    $(this).button('loading')
})
