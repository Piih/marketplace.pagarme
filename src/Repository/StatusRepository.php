<?php

namespace Marketplace\Repository;

use Silex\Application;
use Marketplace\Repository\Interfaces\StatusRepositoryInterface;
use Marketplace\Repository\StatusRepository;
use Marketplace\Entity\Status;

class StatusRepository implements StatusRepositoryInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function create(Status $status)
    {
        $this->conn->createQueryBuilder()
            ->insert('status')
            ->setValue('status', ':status')
            ->setParameter('status', $status->getStatus())
            ->setValue('created_at', ":created_at")
            ->setParameter('created_at', date('Y-m-d H:i:s'))
            ->setValue('updated_at', ":updated_at")
            ->setParameter('updated_at', date('Y-m-d H:i:s'))
            ->execute()
            ;

        return true;
    }

    public function getOne($statusId)
    {
        $status = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('status')
            ->where('status_id = :status_id')
            ->setParameter('status_id', $statusId)
            ->execute()
            ->fetchAll()
            ;

        if(empty($status)) {
            return [];
        }

        $oStatus = new Status();
        $oStatus->fillWithArray($status[0]);
        $oStatus->setRepository($this);
        return $oStatus;
    }

    public function update(Status $status)
    {
        $query = $this->conn->createQueryBuilder()
            ->update('status');

        $oldStatus = $this->getOne($status->getId());

        if(empty($oldStatus)) {
            return false;
        }

        $updated = false;

        if(null !== $status->getStatus()) {
            $query
                ->set('status', ':status')
                ->setParameter('status', $status->getStatus())
                ;
            $updated = true;
        }

        if($updated) {
            $query
                ->set('updated_at', ':updated_at')
                ->setParameter('updated_at', date('Y-m-d H:i:s'))
                ->where('status_id = :status_id')
                ->setParameter('status_id', $status->getId())
                ->execute()
                ;
            return true;
        }

        return false;
    }
}
