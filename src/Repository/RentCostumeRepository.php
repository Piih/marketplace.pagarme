<?php

namespace Marketplace\Repository;

use Silex\Application;
use Marketplace\Repository\Interfaces\RentCostumeRepositoryInterface;
use Marketplace\Entity\RentCostume;
use Marketplace\Entity\Rent;
use Marketplace\Entity\Costume;


class RentCostumeRepository implements RentCostumeRepositoryInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function getRent($rentId)
    {
        $rentRepository = new RentRepository($this->conn);

        $rent = $rentRepository->getOne($rentId);

        if(empty($rent)) {
            return new Rent();
        }

        return $rent;
    }

    public function getCostume($costumeId)
    {
        $costumeRepository = new CostumeRepository($this->conn);

        $costume = $costumeRepository->getOne($costumeId);

        if(empty($costume)) {
            return new Costume();
        }

        return $costume;
    }

    public function create(RentCostume $rentCostume)
    {
        $this->conn->createQueryBuilder()
            ->insert('rent_costume')
            ->setValue('rent_id', ':rent_id')
            ->setParameter('rent_id', $rentCostume->getRentId())
            ->setValue('costume_id', ':costume_id')
            ->setParameter('costume_id', $rentCostume->getCostumeId())
            ->setValue('price', ':price')
            ->setParameter('price', $rentCostume->getPrice())
            ->setValue('quantity', ':quantity')
            ->setParameter('quantity', $rentCostume->getQuantity())
            ->setValue('created_at', ":created_at")
            ->setParameter('created_at', date('Y-m-d H:i:s'))
            ->setValue('updated_at', ":updated_at")
            ->setParameter('updated_at', date('Y-m-d H:i:s'))
            ->execute()
            ;

        return true;
    }

    public function getByRentAndCostume(Rent $rent, Costume $costume)
    {
        $rentCostume = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('rent_costume')
            ->where('rent_id = :rent_id')
            ->setParameter('rent_id', $rent->getId())
            ->andWhere('costume_id = :costume_id')
            ->setParameter('costume_id', $costume->getId())
            ->execute()
            ->fetchAll()
            ;

        if(empty($rentCostume)) {
            return [];
        }

        $oRentCostume = new RentCostume();
        $oRentCostume->fillWithArray($rentCostume[0]);
        $oRentCostume->setRepository($this);

        return $oRentCostume;
    }
}
