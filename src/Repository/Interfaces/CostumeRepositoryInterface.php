<?php

namespace Marketplace\Repository\Interfaces;

use Marketplace\Entity\Costume;

interface CostumeRepositoryInterface
{
    public function create(Costume $costume);
    public function getOne($costumeId);
    public function update(Costume $costume);
    public function remove($costumeId);
}
