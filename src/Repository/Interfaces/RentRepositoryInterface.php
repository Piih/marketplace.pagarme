<?php

namespace Marketplace\Repository\Interfaces;

use Marketplace\Entity\Rent;

interface RentRepositoryInterface
{
    public function getStatus($statusId);
    public function create(Rent $rent);
    public function getOne($rentId);
}
