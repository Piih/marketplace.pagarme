<?php

namespace Marketplace\Repository\Interfaces;

use Marketplace\Entity\Seller;

interface SellerRepositoryInterface
{
    public function create(Seller $seller);
    public function getOne($sellerId);
    public function update(Seller $seller);
    public function remove($selleId);
}
