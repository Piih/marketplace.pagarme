<?php

namespace Marketplace\Repository\Interfaces;

use Marketplace\Entity\RentCostume;
use Marketplace\Entity\Rent;
use Marketplace\Entity\Costume;

interface RentCostumeRepositoryInterface
{
    public function getRent($rentId);
    public function getCostume($costumeId);
    public function create(RentCostume $rentCostume);
    public function getByRentAndCostume(Rent $rent, Costume $costume);
}
