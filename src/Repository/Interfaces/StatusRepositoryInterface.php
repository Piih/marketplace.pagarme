<?php

namespace Marketplace\Repository\Interfaces;

use Marketplace\Entity\Status;

interface StatusRepositoryInterface
{
    public function create(Status $status);
    public function getOne($statusId);
    public function update(Status $status);
}
