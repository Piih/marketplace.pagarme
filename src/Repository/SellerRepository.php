<?php

namespace Marketplace\Repository;

use Silex\Application;
use Marketplace\Repository\Interfaces\SellerRepositoryInterface;
use Marketplace\Entity\Seller;

class SellerRepository implements SellerRepositoryInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function create(Seller $seller)
    {
        $this->conn->createQueryBuilder()
            ->insert('seller')
            ->setValue('seller', ':seller')
            ->setParameter('seller', $seller->getSeller())
            ->setValue('gain_percentage', ':gain_percentage')
            ->setParameter('gain_percentage', $seller->getGainPercentage())
            ->setValue('created_at', ":created_at")
            ->setParameter('created_at', date('Y-m-d H:i:s'))
            ->setValue('updated_at', ":updated_at")
            ->setParameter('updated_at', date('Y-m-d H:i:s'))
            ->execute()
            ;

        return true;
    }

    public function getOne($sellerId)
    {
        $seller = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('seller')
            ->where('seller_id = :seller_id')
            ->setParameter('seller_id', $sellerId)
            ->execute()
            ->fetchAll()
            ;

        if(!empty($seller)) {
            $oSeller = new Seller();
            $oSeller->fillWithArray($seller[0]);
            return $oSeller;
        }

        return [];
    }

    public function update(Seller $seller)
    {
        $query = $this->conn->createQueryBuilder()
            ->update('seller');

        $updated = false;

        if(null !== $seller->getSeller()) {
            $query
                ->set('seller', ':seller')
                ->setParameter('seller', $seller->getSeller())
                ;
            $updated = true;
        }

        if(null !== $seller->getGainPercentage()) {
            $query
                ->set('gain_percentage', ':gain_percentage')
                ->setParameter('gain_percentage', $seller->getGainPercentage())
                ;
            $updated = true;
        }

        if($updated) {
            $query
                ->set('updated_at', ':updated_at')
                ->setParameter('updated_at', date('Y-m-d H:i:s'))
                ->where('seller_id = :seller_id')
                ->setParameter('seller_id', $seller->getId()) 
                ->execute()
                ;
            return true;
        }

        return false;
    }

    public function remove($seller_id)
    {
        $query = $this->conn->createQueryBuilder()
            ->delete('seller')
            ->where('seller_id = ?')
            ->setParameter(0, $seller_id)
            ->execute()
            ;
    }
}
