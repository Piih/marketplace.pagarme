<?php

namespace Marketplace\Repository;

use Silex\Application;
use Marketplace\Repository\Interfaces\RentRepositoryInterface;
use Marketplace\Repository\StatusRepository;
use Marketplace\Entity\Rent;

class RentRepository implements RentRepositoryInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function getStatus($statusId)
    {
        $statusRepository = new StatusRepository($this->conn);

        $status = $statusRepository->getOne($statusId);

        if(empty($status)) {
            return new Status();
        }

        return $status;
    }

    public function create(Rent $rent)
    {
        $this->conn->createQueryBuilder()
            ->insert('rent')
            ->setValue('status_id', ':status_id')
            ->setParameter('status_id', $rent->getStatusId())
            ->setValue('amount', ':amount')
            ->setParameter('amount', $rent->getAmount())
            ->setValue('shipping_price', ':shipping_price')
            ->setParameter('shipping_price', $rent->getShippingPrice())
            ->setValue('created_at', ":created_at")
            ->setParameter('created_at', date('Y-m-d H:i:s'))
            ->setValue('updated_at', ":updated_at")
            ->setParameter('updated_at', date('Y-m-d H:i:s'))
            ->execute()
            ;

        return true;
    }

    public function getOne($rentId)
    {
        $rent = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('rent')
            ->where('rent_id = :rent_id')
            ->setParameter('rent_id', $rentId)
            ->execute()
            ->fetchAll()
            ;

        if(empty($rent)) {
            return [];
        }

        $oRent = new Rent();
        $oRent->fillWithArray($rent[0]);
        $oRent->setRepository($this);
        return $oRent;
    }

    public function getAll($limit = null)
    {
        $query = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('rent')
            ;
        if(is_numeric($limit)) {
            $query->setMaxResults($limit);
        }

        $rents = $query
            ->execute()
            ->fetchAll()
            ;

        if(empty($rents)) {
            return [];
        }

        $rents = array_map(function($rent) {
            $oRent = new Rent();
            $oRent->fillWithArray($rent);
            $oRent->setRepository($this);
            return $oRent;
        }, $rents);

        return $rents;
    }
}
