<?php

namespace Marketplace\Repository;

use Silex\Application;
use Marketplace\Repository\Interfaces\CostumeRepositoryInterface;
use Marketplace\Entity\Costume;
use Marketplace\Entity\Seller;
use Marketplace\Repository\SellerRepository;

class CostumeRepository implements CostumeRepositoryInterface
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function getSeller($sellerId)
    {
        $sellerRepository = new SellerRepository($this->conn);

        $seller = $sellerRepository->getOne($sellerId);

        if(empty($seller)) {
            return new Seller();
        }

        return $seller;
    }

    public function create(Costume $costume)
    {
        $this->conn->createQueryBuilder()
            ->insert('costume')
            ->setValue('costume', ':costume')
            ->setParameter('costume', $costume->getCostume())
            ->setValue('price_rent', ':price_rent')
            ->setParameter('price_rent', $costume->getPriceRent())
            ->setValue('created_at', ":created_at")
            ->setParameter('created_at', date('Y-m-d H:i:s'))
            ->setValue('updated_at', ":updated_at")
            ->setParameter('updated_at', date('Y-m-d H:i:s'))
            ->execute()
            ;

        return true;
    }

    public function getOne($costumeId)
    {
        $costume = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('costume')
            ->where('costume_id = :costume_id')
            ->setParameter('costume_id', $costumeId)
            ->execute()
            ->fetchAll()
            ;

        if(empty($costume)) {
            return [];
        }

        $oCostume = new Costume();
        $oCostume->fillWithArray($costume[0]);
        $oCostume->setRepository($this);
        return $oCostume;
    }

    public function getMany(array $costumesIds)
    {
        $costumes = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('costume')
            ->where('costume_id IN ('.implode(',',$costumesIds).')')
            ->execute()
            ->fetchAll()
            ;

        if(empty($costumes)) {
            return [];
        }

        $costumes = array_map(function($costume) {
            $oCostume = new Costume();
            $oCostume->fillWithArray($costume);
            $oCostume->setRepository($this);
            return $oCostume;
        }, $costumes);

        return $costumes;
    }

    public function getAll($limit = null)
    {
        $query = $this->conn->createQueryBuilder()
            ->select('*')
            ->from('costume')
            ;
        if(is_numeric($limit)) {
            $query->setMaxResults($limit);
        }

        $costumes = $query
            ->execute()
            ->fetchAll()
            ;

        if(empty($costumes)) {
            return [];
        }

        $costumes = array_map(function($costume) {
            $oCostume = new Costume();
            $oCostume->fillWithArray($costume);
            $oCostume->setRepository($this);
            return $oCostume;
        }, $costumes);

        return $costumes;
    }

    public function update(Costume $costume)
    {
        $query = $this->conn->createQueryBuilder()
            ->update('costume');

        $oldCostume = $this->getOne($costume->getId());

        if(empty($oldCostume)) {
            return false;
        }

        $updated = false;

        if(null !== $costume->getCostume()) {
            $query
                ->set('costume', ':costume')
                ->setParameter('costume', $costume->getCostume())
                ;
            $updated = true;
        }

        if(null !== $costume->getPriceRent()) {
            $query
                ->set('price_rent', ':price_rent')
                ->setParameter('price_rent', $costume->getPriceRent())
                ;
            $updated = true;
        }

        if(null !== $costume->getSellerId()) {
            $query
                ->set('seller_id', ':seller_id')
                ->setParameter('seller_id', $costume->getSellerId())
                ;
            $updated = true;
        }

        if($updated) {
            $query
                ->set('updated_at', ':updated_at')
                ->setParameter('updated_at', date('Y-m-d H:i:s'))
                ->where('costume_id = :costume_id')
                ->setParameter('costume_id', $costume->getId())
                ->execute()
                ;
            return true;
        }

        return false;
    }

    public function remove($costume_id)
    {
        $query = $this->conn->createQueryBuilder()
            ->delete('costume')
            ->where('costume_id = ?')
            ->setParameter(0, $costume_id)
            ->execute()
            ;
    }
}
