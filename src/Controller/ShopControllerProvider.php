<?php

namespace Marketplace\Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class ShopControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'Marketplace\Controller\ShopController::index');

        return $controllers;
    }
}
