<?php
namespace Marketplace\Controller;

use Silex\Application;
use Marketplace\Repository\CostumeRepository;
use Symfony\Component\HttpFoundation\Request;

class ShopController
{
    public function index(Application $app)
    {
//        $seller = new Seller();
//        $seller->setSeller('Seller1');
//        $seller->setGainPercentage(85);
//
//        $repository = new SellerRepository($app['db']);
//
//        $repository->create($seller);
        $repository = new CostumeRepository($app['db']);

        $costumes = $repository->getAll();

        return $app['twig']->render('shop/index.twig', ['costumes' => $costumes]);
    }
}
