<?php

namespace Marketplace\Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class CartControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'Marketplace\Controller\CartController::index');
        $controllers->post('/add-costume', 'Marketplace\Controller\CartController::addCostumeAjax');
        $controllers->get('/get-itens', 'Marketplace\Controller\CartController::getItensAjax');
        $controllers->get('/checkout', 'Marketplace\Controller\CartController::checkout');
        $controllers->post('/do-checkout', 'Marketplace\Controller\CartController::doCheckout');
        $controllers->get('/checkout/success', 'Marketplace\Controller\CartController::successCheckout');
        $controllers->get('/checkout/fail', 'Marketplace\Controller\CartController::failCheckout');

        return $controllers;
    }
}
