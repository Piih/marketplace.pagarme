<?php
namespace Marketplace\Controller;

use Silex\Application;
use Marketplace\Repository\CostumeRepository;
use Marketplace\Repository\SellerRepository;
use Marketplace\Entity\Costume;
use Marketplace\Entity\Seller;
use Marketplace\Entity\Cart;
use Marketplace\Entity\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use PagarMe\Sdk\PagarMe;
use PagarMe\Sdk\SplitRule\SplitRuleCollection;

class CartController
{
    public function index(Application $app)
    {
        $cart = new Cart($app['session']);
        $cart->clearCartSession();

        $pagarMe = new PagarMe($app['pagarme_key']);

        return $app['twig']->render('cart/index.twig', []);
    }

    public function addCostumeAjax(Application $app, Request $request)
    {
        $costumeId = $request->request->get('costume-id');

        $cart = new Cart($app['session']);

        $repository = new CostumeRepository($app['db']);

        $costume = $repository->getOne($costumeId);

        $item = new Item($costume);

        $added = $cart->addItem($item);

        $cart->createCartSession();

        return $app->json($added);
    }

    public function getItensAjax(Application $app)
    {
        $cart = new Cart($app['session']);

        return $app->json($cart->getItens());
    }

    public function checkout(Application $app)
    {
        $cart = new Cart($app['session']);

        $repository = new CostumeRepository($app['db']);

        if(null === $cart->getItens()) {
            return $app['twig']->render('cart/checkout.twig', ['costumes' => []]);
        }

        $itens = $cart->getItens();

        $cartSum = $cart->sumItensPrices();

        $cartSumWithShipping = $cart->sumItensPricesWithShipping();

        return $app['twig']->render('cart/checkout.twig', [
            'itens' => $itens,
            'cartSum' => $cartSum,
            'cartSumWithShipping' => $cartSumWithShipping,
            'shippingPrice' => Cart::SHIPPING_PRICE
        ]);
    }

    public function doCheckout(Application $app, Request $request)
    {
        $checkoutData = $request->request->get('Checkout');

        $cart = new Cart($app['session']);

        $pagarMe = new PagarMe($app['pagarme_key']);

        $customer = $this->createCustomer();

        $expirationDate = $checkoutData['month'] . substr($checkoutData['year'], -2);

        $card = $pagarMe->card()->create(
            $checkoutData['card_number'],
            $checkoutData['holder_name'],
            $expirationDate
        );

        $splitRules = $this->createSplitRules($app, $cart, $pagarMe);

        $amount = $cart->sumItensPricesWithShipping();
        $installments = $checkoutData['installments'];
        $capture = true;
        $metadata = ['idProduto' => 13933139];

        $transaction = $pagarMe->transaction()->creditCardTransaction(
            $amount,
            $card,
            $customer,
            $installments,
            $capture,
            null,
            $metadata,
            ["split_rules" => $splitRules]
        );

        if($transaction->getStatus() == 'paid') {
            $cart->clearCartSession();
            return $app->redirect('/cart/checkout/success?transaction_id='.$transaction->getId());
        }

        return $app->redirect('/cart/checkout/fail');
    }

    public function successCheckout(Application $app, Request $request)
    {
        $transactionId = $request->query->get('transaction_id');

        return $app['twig']->render('/cart/success-checkout.twig', [
            'transactionId' => $transactionId
        ]);
    }

    public function failCheckout(Application $app)
    {
    }

    private function createSplitRules(Application $app, Cart $cart, PagarMe $pagarMe)
    {
        $splitRules = new SplitRuleCollection();

        $costumeRepository = new CostumeRepository($app['db']);

        $sellerRepository = new SellerRepository($app['db']);

        $masterSeller = $sellerRepository->getOne(Seller::MASTER_ID);

        foreach($cart->getItens() as $index => $item) {
            $item->getCostume()->setRepository($costumeRepository);

            $seller = $item->getCostume()->seller();

            $seller = $seller->getId() == $masterSeller->getId()
                ? $masterSeller
                : $seller;

            $item->splitProfitForSellers($seller, $masterSeller);

            if($seller->getId() != $masterSeller->getId()) {
                $splitRules[] = $pagarMe->splitRule()->monetaryRule(
                    $seller->profitAmount,
                    $pagarMe->recipient()->get($seller->getPagarmeRecipientId()),
                    true,
                    true
                );
            }
        }

        $splitRules[] = $pagarMe->splitRule()->monetaryRule(
            $masterSeller->profitAmount,
            $pagarMe->recipient()->get($masterSeller->getPagarmeRecipientId()),
            true,
            true
        );

        return $splitRules;
    }

    private function createCustomer()
    {
        $customer = new \PagarMe\Sdk\Customer\Customer(
            [
                'name' => 'John Dove',
                'email' => 'john@site.com',
                'document_number' => '09130141095',
                'address' => new \PagarMe\Sdk\Customer\Address([
                    'street'        => 'rua teste',
                    'street_number' => '42',
                    'neighborhood'  => 'centro',
                    'zipcode'       => '01227200',
                    'complementary' => 'Apto 42',
                    'city'          => 'São Paulo',
                    'state'         => 'SP',
                    'country'       => 'Brasil'
                ]),
                'phone' => new \PagarMe\Sdk\Customer\Phone([
                    'ddd'    => "15",
                    'number' =>"987523421"
                ]),
                'born_at' => '15021994',
                'sex' => 'M'
            ]
        );

        return $customer;
    }
}
