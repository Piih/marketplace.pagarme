<?php

namespace Marketplace\Entity;

class Status extends BaseEntity
{
    protected $status_id;
    protected $status;
    protected $created_at;
    protected $updated_at;

    public function __construct($statusId = null, $status = null)
    {
        $this->status_id = is_null($statusId) ? null : (int) $statusId;
        $this->status = is_null($status) ? null : (string) $status;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return (int)$this->status_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setId($statusId)
    {
        $this->status_id = $statusId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
