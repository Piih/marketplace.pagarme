<?php

namespace Marketplace\Entity;

use Marketplace\Repository\CostumeRepository;

class Seller extends BaseEntity
{
    const MASTER_ID = 1;

    public $profitAmount = 0;

    protected $seller_id;
    protected $seller;
    protected $gain_percentage;
    protected $pagarme_recipient_id;
    protected $created_at;
    protected $updated_at;

    public function __construct($sellerId = null, $seller = null, $gainPercentage = null)
    {
        $this->seller_id = is_null($sellerId) ? null : (int) $sellerId;
        $this->seller = is_null($seller) ? null : (string) $seller;
        $this->gain_percentage = is_null($gainPercentage) ? null : (string) $gainPercentage;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->seller_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setId($sellerId)
    {
        $this->seller_id = $sellerId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getGainPercentage()
    {
        return $this->gain_percentage;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setGainPercentage($gainPercentage)
    {
        $this->gain_percentage = $gainPercentage;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getPagarmeRecipientId()
    {
        return $this->pagarme_recipient_id;
    }
}
