<?php

namespace Marketplace\Entity;

class Rent extends BaseEntity
{
    protected $rent_id;
    protected $status_id;
    protected $amount;
    protected $shippinh_price;
    protected $created_at;
    protected $updated_at;

    public function __construct($rentId = null, $statusId = null, $amount = null, $shippingPrice = null)
    {
        $this->rent_id = is_null($rentId) ? null : (int) $rentId;
        $this->status_id = is_null($statusId) ? null : (string) $statusId;
        $this->amount = is_null($amount) ? null : (int) $amount;
        $this->shipping_price = is_null($shippingPrice) ? null : (int) $shippingPrice;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return (int)$this->rent_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setId($rentId)
    {
        $this->rent_id = $rentId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setStatusId($statusId)
    {
        $this->status_id = $statusId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getAmount()
    {
        return (int)$this->amount;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getShippingPrice()
    {
        return $this->shipping_price;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shipping_price = $shippingPrice;
    }
}
