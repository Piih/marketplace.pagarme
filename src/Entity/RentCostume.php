<?php

namespace Marketplace\Entity;

class RentCostume extends BaseEntity
{
    protected $rent_costume_id;
    protected $rent_id;
    protected $costume_id;
    protected $price;
    protected $quantity;
    protected $created_at;
    protected $updated_at;

    public function __construct($rentCostumeId = null, $rentId = null, $costumeId = null, $price = null, $quantity = null)
    {
        $this->rent_costume_id = is_null($rentCostumeId) ? null : (int) $rentCostumeId;
        $this->rent_id = is_null($rentId) ? null : (int) $rentId;
        $this->costume_id = is_null($costumeId) ? null : (int) $costumeId;
        $this->price = is_null($price) ? null : (int) $price;
        $this->quantity = is_null($quantity) ? null : (int) $quantity;
    }

    public function rent()
    {
        return $this->repository->getRent($this->rent_id);
    }

    public function costume()
    {
        return $this->repository->getCostume($this->costume_id);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->rent_costume_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setId($rentCostumeId)
    {
        $this->rent_costume_id = $rentCostumeId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getRentId()
    {
        return $this->rent_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setRentId($rentId)
    {
        $this->rent_id = $rentId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getCostumeId()
    {
        return $this->costume_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setCostumeId($costumeId)
    {
        $this->costume_id = $costumeId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
