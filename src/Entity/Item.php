<?php

namespace Marketplace\Entity;

use Marketplace\Entity\Costume;
use Marketplace\Entity\Seller;
use Marketplace\Entity\Cart;

class Item
{
    private $costume; 
    private $quantity;
    private $shippingPrice;

    public function __construct(Costume $costume)
    {
        //This is needed because the item serialize in the future
        $costume->setRepository(null);

        $this->costume = $costume;
        $this->quantity = 0;
        $this->shippingPrice = Cart::SHIPPING_PRICE;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getCostume()
    {
        return $this->costume;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;
    }

    public function splitProfitForSellers(Seller $seller, Seller $masterSeller)
    {
        if($seller->getId() == $masterSeller->getId()) {
            $seller->profitAmount += $this->costume->getPriceRent() * $this->quantity + $this->shippingPrice;
            return;
        }

        $masterSeller->profitAmount += $this->costume->getPriceRent() * $this->quantity * (100 - $seller->getGainPercentage()) / 100;
        $seller->profitAmount += $this->costume->getPriceRent() * $this->quantity * $seller->getGainPercentage() / 100 + $this->shippingPrice;
    }
}

