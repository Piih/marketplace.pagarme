<?php

namespace Marketplace\Entity;

use Marketplace\Repository\SellerRepository;
use Marketplace\Entity\Seller;

class Costume extends BaseEntity
{
    protected $costume_id;
    protected $costume;
    protected $price_rent;
    protected $seller_id;
    protected $created_at;
    protected $updated_at;

    public function __construct($costumeId = null, $costume = null, $priceRent = null, $sellerId = null)
    {
        $this->costume_id = is_null($costumeId) ? null : (int) $costumeId;
        $this->costume = is_null($costume) ? null : (string) $costume;
        $this->price_rent = is_null($priceRent) ? null : (int) $priceRent;
        $this->seller_id = is_null($sellerId) ? null : (int) $sellerId;
    }

    public function seller()
    {
        return $this->repository->getSeller($this->seller_id);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->costume_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setId($costumeId)
    {
        $this->costume_id = $costumeId;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getCostume()
    {
        return $this->costume;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setCostume($costume)
    {
        $this->costume = $costume;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getPriceRent()
    {
        return $this->price_rent;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setPriceRent($priceRent)
    {
        $this->price_rent = $priceRent;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSellerId()
    {
        return $this->seller_id;
    }

    /**
     * @codeCoverageIgnore
     */
    public function setSellerId($sellerId)
    {
        $this->seller_id = $sellerId;
    }
}
