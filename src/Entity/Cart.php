<?php

namespace Marketplace\Entity;

use Symfony\Component\HttpFoundation\Session\Session;
use Marketplace\Repository\CostumeRepository;

class Cart
{
    const SHIPPING_PRICE = 4200;

    private $session;

    private $itens;

    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->itens = null !== $this->session->get('cart')
            ? unserialize($this->session->get('cart'))
            : [];
    }

    public function addItem(Item $item)
    {
        $item->setQuantity($item->getQuantity() + 1);

        if(empty($this->getItens())) {
            $this->itens[] = $item;
            return true;
        }

        foreach($this->itens as $oItem) {
            if($oItem->getCostume()->getId() == $item->getCostume()->getId()) {
                return false;
            }
        }

        $this->itens[] = $item;
        return true;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @codeCoverageIgnore
     */
    public function createCartSession()
    {
        $this->session->set('cart', serialize($this->itens));
    }

    /**
     * @codeCoverageIgnore
     */
    public function clearCartSession()
    {
        $this->session->remove('cart');
    }

    public function sumItensPrices()
    {
        $sum = array_reduce($this->itens, function($prev, $current) {
            return $prev + ($current->getCostume()->getPriceRent() * $current->getQuantity());
        });

        return $sum;
    }

    public function sumItensPricesWithShipping()
    {
        $sum = array_reduce($this->itens, function($prev, $current) {
            return $prev + ($current->getCostume()->getPriceRent() * $current->getQuantity() + $current->getShippingPrice());
        });

        return $sum;
    }
}
