<?php

namespace Marketplace\Entity;

class BaseEntity
{
    protected $repository;

    public function fillWithArray(array $array)
    {
        $properties = get_object_vars($this);
        foreach($properties as $name => $value) {
            $this->$name = isset($array[$name]) ? $array[$name] : NULL;
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }
}
